(function(window, document) {
	'use strict';

	function scrollify(id, options) {

		options = [].extend({
			oncontent: false,
			opacity: 0,
			hoverable: true
		}, options);

		var setOptions = function() {
			scrollBar.style.opacity = options.opacity;
			if (options.hoverable) {
				block.onmouseover = function(e) {
					scrollBar.style.opacity = 1;
				}
				block.onmouseout = function(e) {
					scrollBar.style.opacity = options.opacity;
				}
			}
		}

		var reset = function() {
			var scrollWidth = contentBlock.offsetWidth - contentBlock.scrollWidth;
			if (!options.oncontent) {
				contentBlock.style.paddingRight = scrollWidth + 'px';
			}
			contentBlock.style.right = -scrollWidth + 'px';
			sliderHeight = Math.round(scrollBar.offsetHeight / contentBlock.scrollHeight * scrollBar.offsetHeight);
			slider.style.height = sliderHeight + 'px';
			maxScrollTop = contentBlock.scrollHeight - contentBlock.clientHeight;
		}

		var getCurrentTransform = function(elem) {
			var style = elem.style.transform;
			var matches = style.match(/([0-9.]+)/);
			if (matches) {
				return matches[0];
			} else {
				return 0;
			}
		}

		var template = '<div class="scrollify"><div class="scrollify-content">{{ text }}</div><div class="scrollify-bar"><div class="scrollify-slider"></div></div>'
		
		var block = document.getElementById(id);
		block.innerHTML = template.replace('{{ text }}', block.innerHTML);
		
		var contentBlock = document.getElementsByClassName('scrollify-content')[0];
		var scrollBar = document.getElementsByClassName('scrollify-bar')[0];
		var slider = document.getElementsByClassName('scrollify-slider')[0];

		setOptions();
		reset();

		var maxScrollTop;
		var sliderHeight;

		contentBlock.onscroll = function() {
			var transform = (contentBlock.scrollTop * (scrollBar.offsetHeight - sliderHeight)) / maxScrollTop;
			slider.style.transform = 'translateY(' + transform + 'px)';
		};
		
		slider.onmousedown = function(e) {
			var scrollBarCoords = scrollBar.getBoundingClientRect();
			var elemCurrTransform = getCurrentTransform(slider);
			var shiftY = e.pageY - scrollBarCoords.top - elemCurrTransform;
			document.onmousemove = function(e) {
				var transform = e.pageY - scrollBarCoords.top - scrollBar.offsetTop - shiftY;
				var maxTransform = scrollBar.offsetHeight - sliderHeight;
				transform = Math.max(0, transform);
				transform = Math.min(maxTransform, transform);
				slider.style.transform = 'translateY(' + transform + 'px)';
				contentBlock.scrollTop = maxScrollTop * transform / maxTransform;
			}
			document.onmouseup = function(e) {
				document.onmousemove = document.onmouseup = null;
			}
			return false;
		};

		window.onresize = function() {
			reset();
		};

	}
	
	window.scrollify = scrollify;
})(window, document);

Object.prototype.extend = function() {
	if (typeof Object.assign !== 'function') {
		return (function(arguments) {
			var target = arguments[0];
		    if (target === undefined || target === null) {
		        throw new TypeError('Cannot convert undefined or null to object');
		    }
		    var output = Object(target);
		    for (var index = 1; index < arguments.length; index++) {
			    var source = arguments[index];
			    if (source !== undefined && source !== null) {
			        for (var nextKey in source) {
			        	if (source.hasOwnProperty(nextKey)) {
			            	output[nextKey] = source[nextKey];
			            }
			        }
			    }
			}
			return output;
		})(arguments);
	} else {
		return Object.assign.apply(this, arguments);
	}
}